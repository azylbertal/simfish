import numpy as np
import random
import moviepy.editor as mpy
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import os
from time import time
import sys
import json

from simfish_env import SimState

# These functions allows us to update the parameters of our target network with those of the primary network.
def updateTargetGraph(tfVars, tau):
    total_vars = len(tfVars)
    op_holder = []
    for idx, var in enumerate(tfVars[0:total_vars // 2]):
        op_holder.append(tfVars[idx + total_vars // 2].assign(
            (var.value() * tau) + ((1 - tau) * tfVars[idx + total_vars // 2].value())))
    return op_holder


def updateTarget(op_holder, sess):
    for op in op_holder:
        sess.run(op)

# This code allows gifs to be saved of the training episode for use in the Control Center.
def make_gif(images, fname, duration=2, true_image=False, salIMGS=None):

    def make_frame(t):
        try:
            x = images[int(len(images) / duration * t)]
        except:
            x = images[-1]

        if true_image:
            return x.astype(np.uint8)
        else:
            return ((x + 1) / 2 * 255).astype(np.uint8)

    def make_mask(t):
        try:
            x = salIMGS[int(len(salIMGS) / duration * t)]
        except:
            x = salIMGS[-1]
        return x

    clip = mpy.VideoClip(make_frame, duration=duration)
    clip.write_gif(fname, fps=len(images) / duration, verbose=False)

        
class Qnetwork():
    def __init__(self, simulation,rnn_dim, rnn_cell, myScope, num_actions, learning_rate=0.0001):

        #The network recieves the observation from both eyes, processes it
        #through convolutional layers, concatenates it with the internal state
        #and feeds it to the RNN.

        self.num_arms = len(simulation.left_eye.vis_angles) #rays for each eye
        self.rnn_dim = rnn_dim
        self.rnn_output_size = self.rnn_dim
        self.actions = tf.placeholder(shape=[None], dtype=tf.int32, name='actions')
        self.actions_onehot = tf.one_hot(self.actions, num_actions, dtype=tf.float32)

        self.prev_actions = tf.placeholder(shape=[None], dtype=tf.int32, name='prev_actions')
        self.prev_actions_onehot = tf.one_hot(self.prev_actions, num_actions, dtype=tf.float32)
        
        self.internal_state = tf.placeholder(shape=[None, 2], dtype=tf.float32, name='internal_state')

        self.observation = tf.placeholder(shape=[None,3, 2],dtype=tf.float32, name='obs')
        self.reshaped_observation = tf.reshape(self.observation,shape=[-1,self.num_arms,3, 2] )
        self.left_eye = self.reshaped_observation[:,:,:,0]
        self.right_eye = self.reshaped_observation[:,:,:,1]
        self.conv1l = tf.layers.conv1d(inputs=self.left_eye, filters=16, kernel_size=16, strides=4, padding='valid', activation=tf.nn.relu, name=myScope+'_conv1l')
        self.conv2l = tf.layers.conv1d(inputs=self.conv1l, filters=8, kernel_size=8, strides=2, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv2l')
        self.conv3l = tf.layers.conv1d(inputs=self.conv2l, filters=8, kernel_size=4, strides=1, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv3l')
        self.conv4l = tf.layers.conv1d(inputs=self.conv3l, filters=64, kernel_size=4, strides=1, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv4l')

        self.conv1r = tf.layers.conv1d(inputs=self.right_eye, filters=16, kernel_size=16, strides=4, padding='valid', activation=tf.nn.relu, name=myScope+'_conv1r')
        self.conv2r = tf.layers.conv1d(inputs=self.conv1r, filters=8, kernel_size=8, strides=2, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv2r')
        self.conv3r = tf.layers.conv1d(inputs=self.conv2r, filters=8, kernel_size=4, strides=1, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv3r')
        self.conv4r = tf.layers.conv1d(inputs=self.conv3r, filters=64, kernel_size=4, strides=1, padding = 'valid', activation=tf.nn.relu, name=myScope+'_conv4r')

        self.exp_keep = tf.placeholder(shape=None,dtype=tf.float32)
        self.Temp = tf.placeholder(shape=None,dtype=tf.float32)

        self.trainLength = tf.placeholder(dtype=tf.int32)
        # We take the output from the final convolutional layer and send it to a recurrent layer.
        # The input must be reshaped into [batch x trace x units] for rnn processing,
        # and then returned to [batch x units] when sent through the upper levles.

        self.batch_size = tf.placeholder(dtype=tf.int32, shape=[])
        self.conv4l_flat = tf.layers.flatten(self.conv4l)
        self.conv4r_flat = tf.layers.flatten(self.conv4r)

        self.conv_with_states = tf.concat([self.conv4l_flat, self.conv4r_flat, self.prev_actions_onehot, self.internal_state], 1)
        self.rnn_in = tf.layers.dense(self.conv_with_states, self.rnn_dim, activation=tf.nn.relu, kernel_initializer=tf.orthogonal_initializer,
                                      trainable=True)
        self.convFlat = tf.reshape(self.rnn_in, [self.batch_size, self.trainLength, self.rnn_dim])

        self.state_in = rnn_cell.zero_state(self.batch_size, tf.float32)
        self.rnn, self.rnn_state = tf.nn.dynamic_rnn(inputs=self.convFlat, cell=rnn_cell, dtype=tf.float32, initial_state=self.state_in, scope=myScope + '_rnn')
        self.rnn = tf.reshape(self.rnn, shape=[-1, self.rnn_dim])
        self.rnn_output = self.rnn

        # The output from the recurrent player is then split into separate Value and Advantage streams
        self.streamA, self.streamV = tf.split(self.rnn_output, 2, 1)
        self.AW = tf.Variable(tf.random_normal([(self.rnn_output_size) // 2, num_actions]))
        self.VW = tf.Variable(tf.random_normal([(self.rnn_output_size) // 2, 1]))
        self.Advantage = tf.matmul(self.streamA, self.AW)
        self.Value = tf.matmul(self.streamV, self.VW)

        self.salience = tf.gradients(self.Advantage, self.observation)
        # Then combine them together to get our final Q-values.
        self.Qout = self.Value + tf.subtract(self.Advantage, tf.reduce_mean(self.Advantage, axis=1, keep_dims=True))
        self.predict = tf.argmax(self.Qout, 1)
        self.Q_dist = tf.nn.softmax(self.Qout/self.Temp)
        # Below we obtain the loss by taking the sum of squares difference between the target and prediction Q values.
        self.targetQ = tf.placeholder(shape=[None], dtype=tf.float32)


        self.Q = tf.reduce_sum(tf.multiply(self.Qout, self.actions_onehot), axis=1)

        self.td_error = tf.square(self.targetQ - self.Q)

        # In order to only propogate accurate gradients through the network, we will mask the first
        # half of the losses for each trace as per Lample & Chatlot 2016
        self.maskA = tf.zeros([self.batch_size, self.trainLength // 2])
        self.maskB = tf.ones([self.batch_size, self.trainLength // 2])
        self.mask = tf.concat([self.maskA, self.maskB], 1)
        self.mask = tf.reshape(self.mask, [-1])
        self.loss = tf.reduce_mean(self.td_error * self.mask)

        self.trainer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        self.updateModel = self.trainer.minimize(self.loss)


class experience_buffer():
    def __init__(self, buffer_size=4000):
        self.buffer = []
        self.buffer_size = buffer_size

    def add(self, experience):
        if len(self.buffer) + 1 >= self.buffer_size:
            self.buffer[0:(1 + len(self.buffer)) - self.buffer_size] = []
        self.buffer.append(experience)

    def sample(self, batch_size, trace_length):
        #print(len(self.buffer), batch_size)
        sampled_episodes = random.sample(self.buffer, batch_size)
        sampledTraces = []
        for episode in sampled_episodes:
            point = np.random.randint(0, len(episode) + 1 - trace_length)
            sampledTraces.append(episode[point:point + trace_length])
        sampledTraces = np.array(sampledTraces)
        return np.reshape(sampledTraces, [batch_size * trace_length, 6])

if __name__ == "__main__":
    #Setting the training parameters
    
    base_name = sys.argv[1]
    
    with open(base_name + '_learning.json', 'r') as f:
        params = json.load(f)

    with open(base_name + '_env.json', 'r') as f:
        env = json.load(f)

    sim = SimState(env)        
    
    times=[]

    # We define the cells for the primary and target q-networks
    cell = tf.nn.rnn_cell.LSTMCell(num_units=params['rnn_dim'], state_is_tuple=True)
    cellT = tf.nn.rnn_cell.LSTMCell(num_units=params['rnn_dim'], state_is_tuple=True)
    mainQN = Qnetwork(sim, params['rnn_dim'], cell, 'main', params['num_actions'], learning_rate=params['learning_rate'])
    targetQN = Qnetwork(sim, params['rnn_dim'], cellT, 'target', params['num_actions'], learning_rate=params['learning_rate'])



    init = tf.global_variables_initializer()

    saver = tf.train.Saver(max_to_keep=5)

    trainables = tf.trainable_variables()

    targetOps = updateTargetGraph(trainables, params['tau'])

    myBuffer = experience_buffer(buffer_size=params['exp_buffer_size'])
    frame_buffer = []
    save_frames = False
    # Set the rate of random action decrease.
    e = params['startE']
    stepDrop = (params['startE'] - params['endE']) / params['anneling_steps']

    # create lists to contain total rewards and steps per episode
    rList = []
    total_steps = 0

    # Make a path for our model to be saved in.
    path = './' + base_name + '_output'
    if not os.path.exists(path):
        os.makedirs(path)
        os.makedirs(path+'/episodes')
        os.makedirs(path+'/logs')
        load_model = False
    else:
        load_model = True

    ##Write the first line of the master log-file for the Control Center

    with tf.Session() as sess:
        if load_model == True:
            print('Loading Model...')
            ckpt = tf.train.get_checkpoint_state(path)
            print(ckpt)
            saver.restore(sess, ckpt.model_checkpoint_path)
        else:
            sess.run(init)

        updateTarget(targetOps, sess)  # Set the target network to be equal to the primary network.

        writer = tf.summary.FileWriter(path+'/logs/', tf.get_default_graph())
        for i in range(params['num_episodes']):

            t0 = time()
            episodeBuffer = []
            env_frames = []
            sim.reset()
            sa = np.zeros((1, 128))
            sv = np.zeros((1, 128))
            s, r, internal_state, d, frame_buffer = sim.simulation_step(3, frame_buffer=frame_buffer, save_frames=save_frames, activations=(sa,))
            rAll = 0
            j = 0
            state = (np.zeros([1, mainQN.rnn_dim]), np.zeros([1, mainQN.rnn_dim]))  # Reset the recurrent layer's hidden state
            a = 0
            all_actions = []
        # The Q-Network
            while j < params['max_epLength']:
                j += 1
                # Choose an action by greedily (with e chance of random action) from the Q-network
                if np.random.rand(1) < e or total_steps < params['pre_train_steps']:
                    [state1, sa, sv] = sess.run([mainQN.rnn_state, mainQN.streamA, mainQN.streamV],
                                      feed_dict={mainQN.observation: s, mainQN.internal_state: internal_state, mainQN.prev_actions: [a], mainQN.trainLength: 1,
                                                 mainQN.state_in: state, mainQN.batch_size: 1, mainQN.exp_keep:1.0})
                    a = np.random.randint(0, params['num_actions'])
                else:
                    a, state1, sa, sv = sess.run([mainQN.predict, mainQN.rnn_state, mainQN.streamA, mainQN.streamV], \
                                         feed_dict={mainQN.observation: s, mainQN.internal_state: internal_state, mainQN.prev_actions: [a], mainQN.trainLength: 1,
                                                    mainQN.state_in: state, mainQN.batch_size: 1, mainQN.exp_keep:1.0})
                    a = a[0]

                all_actions.append(a)
                s1, r, internal_state, d, frame_buffer = sim.simulation_step(a, frame_buffer=frame_buffer, save_frames=save_frames, \
                                                                             activations=(sa,))
                total_steps += 1
                episodeBuffer.append(np.reshape(np.array([s, a, r, internal_state, s1, d]), [1, 6]))
                if total_steps > params['pre_train_steps']:
                    if e > params['endE']:
                        e -= stepDrop

                    if total_steps % (params['update_freq']) == 0:
                        updateTarget(targetOps, sess)
                        # Reset the recurrent layer's hidden state
                        state_train = (np.zeros([params['batch_size'], mainQN.rnn_dim]), np.zeros([params['batch_size'], mainQN.rnn_dim]))

                        trainBatch = myBuffer.sample(params['batch_size'], params['trace_length'])  # Get a random batch of experiences.
                        # Below we perform the Double-DQN update to the target Q-values
                        Q1 = sess.run(mainQN.predict, feed_dict={ \
                            mainQN.observation: np.vstack(trainBatch[:, 4]), mainQN.prev_actions: np.hstack(([0], trainBatch[:-1, 1])), \
                            mainQN.trainLength: params['trace_length'], mainQN.internal_state: np.vstack(trainBatch[:, 3]), mainQN.state_in: state_train, mainQN.batch_size: params['batch_size'], mainQN.exp_keep:1.0})
                        Q2 = sess.run(targetQN.Qout, feed_dict={ \
                            targetQN.observation: np.vstack(trainBatch[:, 4]), targetQN.prev_actions: np.hstack(([0], trainBatch[:-1, 1])),\
                            targetQN.trainLength: params['trace_length'], targetQN.internal_state: np.vstack(trainBatch[:, 3]), targetQN.state_in: state_train,
                            targetQN.batch_size: params['batch_size'], targetQN.exp_keep:1.0})
                        end_multiplier = -(trainBatch[:, 5] - 1)
                        
                        doubleQ = Q2[range(params['batch_size'] * params['trace_length']), Q1]
                        targetQ = trainBatch[:, 2] + (params['y'] * doubleQ * end_multiplier)
                        # Update the network with our target values.
                        sess.run(mainQN.updateModel, \
                                 feed_dict={mainQN.observation: np.vstack(trainBatch[:, 0]),
                                            mainQN.targetQ: targetQ, \
                                            mainQN.actions: trainBatch[:, 1], mainQN.internal_state: np.vstack(trainBatch[:, 3]), \
                                            mainQN.prev_actions: np.hstack(([3], trainBatch[:-1, 1])), mainQN.trainLength: params['trace_length'], \
                                            mainQN.state_in: state_train, mainQN.batch_size: params['batch_size'], mainQN.exp_keep:1.0})
                rAll += r
                s = s1
                state = state1
                if d == True:
                    break

            # Add the episode to the experience buffer
            print('episode ' + str(i) + ': num steps = ' + str(sim.num_steps), flush=True)
            if not save_frames:
                times.append(time() - t0)
            episode_summary = tf.Summary(value=[tf.Summary.Value(tag="episode reward", simple_value=rAll)])
            writer.add_summary(episode_summary, total_steps)

            for act in range(params['num_actions']):
                action_freq = np.sum(np.array(all_actions)==act) / len(all_actions)
                afreq = tf.Summary(value=[tf.Summary.Value(tag="action "+str(act), simple_value=action_freq)])
                writer.add_summary(afreq, total_steps)


            bufferArray = np.array(episodeBuffer)
            episodeBuffer = list(zip(bufferArray))
            myBuffer.add(episodeBuffer)
            rList.append(rAll)
            # Periodically save the model.
            if i % params['summaryLength'] == 0 and i != 0:
                print('mean time:')
                print(np.mean(times))
                saver.save(sess, path + '/model-' + str(i) + '.cptk')

                print("Saved Model")
                print(total_steps, np.mean(rList[-50:]), e)
                print(frame_buffer[0].shape)
                make_gif(frame_buffer, path + '/episodes/episode-'+str(i)+'.gif', duration=len(frame_buffer) * params['time_per_step'], true_image=True)
                frame_buffer = []
                save_frames = False

            if (i+1) % params['summaryLength'] == 0:
                print('starting to save frames', flush=True)
                save_frames = True

